# Septerra Core Linux Script

Used with the GOG version of Septerra Core.

This is my solution to the game hanging or freezing after combat bug that's been noted at [WineHQ](https://appdb.winehq.org/objectManager.php?sClass=version&iId=33876) where it's said...

>Task-switching to another app will fix this.

This still leaves the game mostly unplayable. Instead, having panning enabled with `xrandr` you can simply pan the desktop a little to force the frames to advance.

In the script `septerra_mover` is just a utility used to automatically and accurately move the window to a corner. See [winmover](https://gitlab.com/teerl/winmover) if you're really interested.
