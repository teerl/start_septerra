#!/bin/bash
wine explorer /desktop=septerra.exe,640x480 septerra.exe -r &
WINEPID=$!
sleep 1
./septerra_mover
sleep 1
GAMEREZ=640x480
SCREENREZ=$(xrandr | grep -F '*' | awk '{print $1;}')
xrandr --output DVI-D-0 --mode ${GAMEREZ} --panning ${SCREENREZ}
wait $WINEPID
sleep 3
xrandr --output DVI-D-0 --mode ${SCREENREZ}
